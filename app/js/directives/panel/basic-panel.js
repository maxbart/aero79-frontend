angular
    .module('app')
    .directive('basicPanel', function ($sce) {
        return {
            restrict: 'E',
            transclude: {
                'menu': '?panelMenu',
                'body': 'panelBody'
            },
            scope: {},
            link: function (scope, elem, attr) {
                scope.attrs = attr;
                // scope.menu = $sce.trustAsHtml($(elem).find("."+attr.menuClass).detach().html());
                scope.attrs.showTitle = attr.showTitle != "false" ? true : false;
                scope.attrs.size = scope.attrs.size < 1 || scope.attrs.size > 12 || !scope.attrs.size ? 6 : scope.attrs.size;
            },
            templateUrl: 'js/directives/panel/basic-panel.html'
        };
    });