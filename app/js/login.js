/**
 * Created by Max on 25/09/16.
 */
$(function () {
    $('#loginForm').on('blur', 'input[required]', validator.checkField);

    $("#loginButton").click(function (e) {
        var form = $("#loginForm");

        e.preventDefault();
        var submit = true;

        // evaluate the form using generic validating
        if (!validator.checkAll(form)) {
            submit = false;
        }

        if (submit) {
            authenticate($("#email").val(), $("#password").val(), "/aero79-frontend/app/");
        }
        return false;

    });

    // if (isLoggedIn()){
    //     window.location.href = "/";
    // }

});