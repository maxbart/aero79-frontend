/**
 * Created by Max on 25/09/16.
 */

(function () {
    'use strict';

    angular
        .module('app')
        .factory('WebsocketService', WebsocketService);
    WebsocketService.$inject = ['$rootScope', '$http', '$API'];
    function WebsocketService($rootScope, $http, $API) {
        // var service = {};

        // service.GetAll = GetAll;
        // service.GetById = GetById;
        // service.GetByUsername = GetByUsername;
        // service.Create = Create;
        // service.Update = Update;
        // service.Delete = Delete;
        // service.Count = Count;

        // return service;

        var sock = new SockJS($API.UserModuleBase + '/stomp');

        var client = Stomp.over(sock);

        client.connect({}, function () {
            client.subscribe("/topic/um/updateUser", function (message) {
                console.log("message", message);
                $rootScope.$broadcast('refreshUsers');
            });

            client.subscribe("/topic/um/newUser", function (message) {
                console.log("message", message);
                $rootScope.$broadcast('refreshUsers');
            });

            client.subscribe("/topic/um/deleteUser", function (message) {
                console.log("message", message);
                $rootScope.$broadcast('refreshUsers');
            });
        });

        // var websocket = new WebSocket(websocket_url);

        //
        // websocket.onmessage = function(msg) {
        //     items.push(JSON.parse(msg.data));
        //     $rootScope.$broadcast('new_message');
        // };

        return {
            // fetchItems: function() {
            //     return items;
            // }
        }
    }

})();