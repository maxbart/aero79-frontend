/**
 * Created by Max on 25/09/16.
 */

(function () {
    'use strict';

    angular
        .module('app', ['ngRoute', 'datatables', '720kb.datepicker', 'ui.codemirror', 'pascalprecht.translate', 'mgo-angular-wizard', 'ui.mask'])
        .config(config)
        .config(['$translateProvider', function ($translateProvider) {
            // add translation table
            $translateProvider
                .translations('nl', i18n_nl)
                .translations('en', i18n_en)
                .preferredLanguage('nl');
        }])
        .constant("$API", {
            UserModuleBase: "http://localhost:8080",
            UserModule: "http://localhost:8080/api/um/v1",
            UtilModule: "http://localhost:8082/api/util/v1"
        })
        .run(run);

    config.$inject = ['$routeProvider', '$locationProvider'];
    function config($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                controller: 'DashboardController',
                templateUrl: 'views/dashboard/dashboard.html',
                controllerAs: 'vm'
            })
            .when('/users', {
                controller: 'UserController',
                templateUrl: 'views/users/index.html',
                controllerAs: 'vm'
            })
            .when('/users/:id', {
                controller: 'UserProfileController',
                templateUrl: 'views/users/profile.html',
                controllerAs: 'vm'
            })
            .when('/create/user', {
                controller: 'UserCreateController',
                templateUrl: 'views/users/create.html',
                controllerAs: 'vm'
            })
            .when('/membercards', {
                controller: 'MembersCardController',
                templateUrl: 'views/membersCard/index.html',
                controllerAs: 'vm'
            })
            .when('/profile/:id', {
                controller: 'UserProfileController',
                templateUrl: 'views/users/profile.html',
                controllerAs: 'vm'
            })
            .when('/register', {
                controller: 'RegisterController',
                templateUrl: 'views/register/register.html',
                controllerAs: 'vm'
            })
            .otherwise({redirectTo: '/login'});
    }

    run.$inject = ['$rootScope', '$location', '$http', '$API', 'DTDefaultOptions', '$translate'];
    function run($rootScope, $location, $http, $API, DTDefaultOptions, $translate) {
        $http.defaults.headers.common['X-Auth-Token'] = localStorage.getItem('X-Auth-Token');

        isLoggedIn();

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // console.log("loggedIn: ", isLoggedIn());
            isLoggedIn();
            $http.defaults.headers.common['X-Auth-Token'] = localStorage.getItem('X-Auth-Token');

            // redirect to login page if not logged in and trying to access a restricted page
            var restrictedPage = $.inArray($location.path(), ['/users', '/']) === -1;
            // var loggedIn = $rootScope.globals.currentUser;
            if (restrictedPage) {
                // $location.path('/login');
            }
        });
    }
})();

