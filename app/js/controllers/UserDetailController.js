/**
 * Created by Bart on 25/10/16.
 */
(function () {
    'use strict';

    angular
        .module('app')
        .controller('UserDetailController', UserDetailController);

    UserDetailController.$inject = ['UserService', '$scope', 'WebsocketService', '$rootScope', '$q', '$filter', '$routeParams'];
    function UserDetailController(UserService, $scope, WebsocketService, $rootScope, $q, $filter, $routeParams) {
        var vm = this;

        vm.userId = $routeParams.id;
        vm.user = null;
        // vm.deleteUser = deleteUser;

        initController();

        function initController() {
            getUserDetails(vm.userId);
        }

        function getUserDetails(id) {
            UserService.GetById(id)
                .then(function (user) {
                    vm.user = user;
                    console.log(vm.user);
                });
        }

        function deleteUser(id) {
            UserService.Delete(id)
                .then(function () {
                    loadAllUsers();
                });
        }
    }

})();