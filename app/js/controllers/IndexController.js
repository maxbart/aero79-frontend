/**
 * Created by Max on 25/09/16.
 */
(function () {
    'use strict';

    angular
        .module('app')
        .controller('IndexController', IndexController);

    IndexController.$inject = ['UserService', '$rootScope', '$scope', 'WebsocketService', '$translate'];
    function IndexController(UserService, $rootScope, $scope, WebsocketService, $translate) {
        var vm = this;

        vm.user = null;
        vm.changeLanguage = changeLanguage;

        initController();

        $scope.$watch(function () {
            return localStorage.getItem('user');
        }, function (newVal, oldVal) {
            if (oldVal !== newVal) {
                vm.user = JSON.parse(newVal);
            }
        });

        function initController() {
            // loadAllUsers();
            // countUsers();
            loadCurrentUser();


        }


        function loadCurrentUser() {
            UserService.GetByEmail(JSON.parse(localStorage.getItem('user')).email)
                .then(function (user) {
                    vm.user = user;
                    $rootScope.user = vm.user;
                    localStorage.setItem('user', JSON.stringify(user));
                });
        }

        function loadAllUsers() {
            UserService.GetAll()
                .then(function (users) {
                    vm.allUsers = users;
                });
        }

        function countUsers() {
            UserService.Count('/enabled/1')
                .then(function (result) {
                    vm.userCount = result.count;
                });
        }

        function deleteUser(id) {
            UserService.Delete(id)
                .then(function () {
                    loadAllUsers();
                });
        }

        function changeLanguage(langKey) {
            $translate.use(langKey);
        }
    }

})();
