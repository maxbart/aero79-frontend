/**
 * Created by Max on 25/09/16.
 */

(function () {
    'use strict';

    angular
        .module('app')
        .factory('ParamService', ParamService);
    ParamService.$inject = ['$rootScope', '$http', '$API'];
    function ParamService($rootScope, $http, $API) {
        var service = {};
        var ext = ".json";

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.GetByLabel = GetByLabel;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        service.Count = Count;
        service.hasRole = hasRole;
        service.findProperty = findProperty;

        return service;

        function GetAll() {
            return $http.get($API.UtilModule + '/user' + ext).then(handleSuccess, handleError('Error getting all users'));
        }

        function GetById(id) {
            return $http.get($API.UtilModule + '/user/' + id + ext).then(handleSuccess, handleError('Error getting user by id'));
        }


        function GetByLabel(label) {
            return $http.get($API.UtilModule + '/param/' + encodeURIComponent(label) + '/byLabel' + ext).then(handleSuccess, handleError('Error getting parameter by label'));
        }

        function Create(parameter) {
            return $http.post($API.UtilModule + '/param', parameter).then(handleSuccess, handleError('Error creating parameter'));
        }

        function Update(parameter) {
            return $http.put($API.UtilModule + '/param/' + parameter.id, parameter).then(handleSuccess, handleError('Error updating parameter'));
        }

        function Delete(id) {
            return $http.delete('/api/users/' + id).then(handleSuccess, handleError('Error deleting user'));
        }

        function Count(params) {
            return $http.get($API.url + $API.um + '/user/count' + params + ext).then(handleSuccess, handleError('Error counting all users'));
        }

        function hasRole(user, role) {
            for (var x in user.role) {
                if (user.role[x].authority.toLowerCase() === role.toLowerCase()) {
                    return true;
                }
            }
            return false;
        }

        function findProperty(user, prop) {
            for (var x in user.property) {
                if (user.property[x] && user.property[x].label.toLowerCase() === prop.toLowerCase()) {
                    return user.property[x].value;
                }
            }
            return null;
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return {success: false, message: error};
            };
        }
    }

})();
