angular
    .module('app')
    .directive('formWizard', function ($sce) {
        return {
            restrict: 'C',
            scope: {
                options: "=options"
            },
            link: function (scope, elem, attr) {
                scope.$watch('options', function (options) {
                    var element = $(elem);
                    element.smartWizard(options);


                    var buttonPrevious = element.find(".buttonPrevious");
                    var buttonNext = element.find(".buttonNext");
                    var buttonFinish = element.find(".buttonFinish");

                    buttonPrevious.addClass('btn btn-default');
                    buttonNext.addClass('btn btn-primary');

                    if (!options['includeFinishButton']) {
                        buttonFinish.remove();
                    }

                    if (options['reverseButtonsOrder']) {
                        buttonPrevious.after(buttonNext);
                    }

                });


            }
        };
    });