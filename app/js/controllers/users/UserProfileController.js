/**
 * Created by Bart on 25/10/16.
 */
(function () {
    'use strict';

    angular
        .module('app')
        .controller('UserProfileController', UserProfileController);

    UserProfileController.$inject = ['UserService', 'AddressService', 'PropertyService', '$scope', 'WebsocketService', '$rootScope', '$q', '$filter', '$routeParams', 'ParamService'];
    function UserProfileController(UserService, AddressService, PropertyService, $scope, WebsocketService, $rootScope, $q, $filter, $routeParams, ParamService) {
        var vm = this;

        vm.userId = $routeParams.id;
        vm.curentUser = JSON.parse(localStorage.getItem('user'));
        vm.edit = false;
        vm.deleteUser = deleteUser;
        vm.editProfile = editProfile;
        vm.saveProfile = saveProfile;
        vm.findProp = findProp;
        vm.cancelEditProfile = cancelEditProfile;
        vm.user = null;

        vm.cropperOptions = {
            aspectRatio: 4 / 4,
            dragMode: 'move',
            autoCropArea: 1,
            zoomOnTouch: false,
            zoomOnWheel: false
        };

        vm.editableUser = null;

        if (vm.curentUser.id == vm.userId) {
            vm.user = JSON.parse(localStorage.getItem('user'));
        }

        initController();

        function initController() {
            vm.allowed = $routeParams.id == vm.curentUser.id || UserService.hasRole(vm.curentUser, 'ROLE_ADMIN');
            if (!vm.user) {
                getUserDetails(vm.userId);
            }
        }

        function getUserDetails(id) {
            UserService.GetById(id)
                .then(function (user) {
                    vm.user = user;
                    // vm.allowed = vm.allowed || UserService.hasRole(user, 'ROLE_ADMIN');
                    console.log("Find property 'brevet': ", UserService.findProperty(user, 'brevet'));
                });
        }

        function deleteUser(id) {
            UserService.Delete(id)
                .then(function () {
                    window.history.back();
                });
        }

        function editProfile() {
            vm.edit = true;
            vm.editableUser = angular.copy(vm.user);
            vm.editableUser.property = {};
            var membershipType = UserService.findProperty(vm.user, 'membershipType');
            vm.editableUser.property.membershipType = membershipType ? membershipType : {value: '', type: 'String', label: 'membershipType'};
            var brevet = UserService.findProperty(vm.user, 'brevet');
            vm.editableUser.property.brevet = brevet ? brevet : {value: '', type: 'String', label: 'brevet'};
            vm.editableUser.dateOfBirth = moment(vm.editableUser.dateOfBirth).format('DD/MM/YYYY');
            vm.cropperOptions.initImg = vm.editableUser.photo;
            console.log("edit", vm.editableUser);
        }

        $rootScope.$on('crop', function (e, args) {
            vm.editableUser.photo = args.crop;
        });

        function saveProfile() {
            vm.editableUser.dateOfBirth = moment(vm.editableUser.dateOfBirth, 'DD/MM/YYYY').format('YYYY-MM-DD 00:00:00');
            vm.user = angular.copy(vm.editableUser);


            UserService.Update(vm.user)
                .then(function (response) {
                    vm.user.dateOfBirth = moment(vm.user.dateOfBirth).toISOString();

                    if (vm.userId == vm.curentUser.id) {
                        localStorage.setItem('user', JSON.stringify(vm.user));
                    }
                    vm.editableUser = null;
                    vm.edit = false;
                });

            //update address
            for (var i in vm.user.address) {
                AddressService.Update(vm.user.address[i])
                    .then(function (response) {
                        console.log("Stored Address", vm.user.address[i]);
                    });
            }

            for (var label in vm.user.property) {
                PropertyService.CreateOrUpdate(vm.user.property[label], vm.user).then(function (response) {
                    console.log(response);
                });
            }
        }

        function findProp(object, key) {
            return ParamService.findProperty(object, key);
        }

        function cancelEditProfile() {
            vm.edit = false;
            vm.editableUser = null;
        }

    }

})();