/**
 * Created by Max on 25/09/16.
 */
(function () {
    'use strict';

    angular
        .module('app')
        .controller('UserController', UserController);

    UserController.$inject = ['UserService', 'ParamService', 'TemplateService', '$scope', 'WebsocketService', '$rootScope', '$q', 'DTOptionsBuilder', 'DTColumnBuilder', '$filter', '$location', '$translate'];
    function UserController(UserService, ParamService, TemplateService, $scope, WebsocketService, $rootScope, $q, DTOptionsBuilder, DTColumnBuilder, $filter, $location, $translate) {
        var vm = this;
        vm.printMembercards = printMembercards;
        vm.printMemberList = printMemberList;
        vm.memberCard;
        vm.memberList;

        vm.dtInstance = {};

        vm.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
            var defer = $q.defer();

            UserService.GetAll().then(function (result) {
                defer.resolve(result);
            });
            return defer.promise;
        })
            .withPaginationType('simple_numbers')
            .withOption('rowCallback', rowCallback)
            .withLanguageSource('js/i18n/datatables/' + $translate.use() + '.json');

        vm.dtColumns = [
            DTColumnBuilder.newColumn('identification'),
            DTColumnBuilder.newColumn('firstName'),
            DTColumnBuilder.newColumn('prefixLastName'),
            DTColumnBuilder.newColumn('lastName')
        ];

        function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            // Unbind first in order to avoid any duplicate handler (see https://github.com/l-lin/angular-datatables/issues/87)
            $('td', nRow).unbind('click');
            $('td', nRow).bind('click', function () {
                $scope.$apply(function () {
                    $location.path($location.$$path + '/' + aData.id);
                    // $location.path('/profile/' + aData.id);
                });
            });
            return nRow;
        }

        $scope.$on('refreshUsers', function () {
            vm.dtInstance.reloadData();
        });

        $rootScope.$on('$translateChangeSuccess', function () {
            vm.dtOptions.withLanguageSource("js/i18n/datatables/" + $translate.use() + ".json");
            // vm.dtInstance.rerender();
        });

        // vm.user = null;
        vm.allUsers = [];
        // vm.deleteUser = deleteUser;

        initController();

        function initController() {
            loadAllUsers();
            loadCurrentMemberCardTemplate();
            loadCurrentMemberListTemplate();
        }

        function loadCurrentUser() {
            UserService.GetByUsername($rootScope.globals.currentUser.username)
                .then(function (user) {
                    vm.user = user;
                });
        }

        function loadAllUsers() {
            UserService.GetAll()
                .then(function (users) {
                    vm.allUsers = users;
                });
        }

        function deleteUser(id) {
            UserService.Delete(id)
                .then(function () {
                    loadAllUsers();
                });
        }

        function loadCurrentMemberCardTemplate() {
            ParamService.GetByLabel("template")
                .then(function (param) {
                    vm.memberCard = param.value;
                });
        }

        function printMembercards() {

            var html = "";
            for (var i in vm.allUsers) {
                vm.allUsers[i].photo = vm.allUsers[i].photo ? vm.allUsers[i].photo : defaultAvatar;
                html += TemplateService.parseHTML(vm.memberCard, {user: vm.allUsers[i], logo: "http://www.aero79.nl/images/logoAero.png"});
            }
            openPrintDialog(html);
        }

        function loadCurrentMemberListTemplate() {
            ParamService.GetByLabel("memberListTemplate")
                .then(function (param) {
                    vm.memberList = param.value;
                });
        }

        function printMemberList() {

            var html = "";
            for (var i in vm.allUsers) {
                vm.allUsers[i].photo = vm.allUsers[i].photo ? vm.allUsers[i].photo : defaultAvatar;
                html += TemplateService.parseHTML(vm.memberList, {user: vm.allUsers[i], logo: "http://www.aero79.nl/images/logoAero.png"});
            }
            openPrintDialog(html);
        }

        function openPrintDialog(html) {
            var w = window.open();

            $(w.document.head).html('<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">');
            $(w.document.body).html(html);
            setTimeout(function () {
                w.print();
                w.close();
            }, 500);
        }
    }

})();
