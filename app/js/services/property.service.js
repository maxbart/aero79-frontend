/**
 * Created by Max on 25/09/16.
 */

(function () {
    'use strict';

    angular
        .module('app')
        .factory('PropertyService', PropertyService);
    PropertyService.$inject = ['$rootScope', '$http', '$API'];
    function PropertyService($rootScope, $http, $API) {
        var service = {};
        var ext = ".json";

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.Create = Create;
        service.Update = Update;
        service.CreateOrUpdate = CreateOrUpdate;
        service.Delete = Delete;
        service.Count = Count;
        service.hasRole = hasRole;

        return service;

        function GetAll() {
            return $http.get($API.UserModule + '/address' + ext).then(handleSuccess, handleError('Error getting all users'));
        }

        function GetById(id) {
            return $http.get($API.UserModule + '/address/' + id + ext).then(handleSuccess, handleError('Error getting user by id'));
        }

        function Create(address) {
            return $http.post($API.UserModule + '/address', address).then(handleSuccess, handleError('Error creating user'));
        }

        function Update(address) {
            return $http.put($API.UserModule + '/address/' + address.id, address).then(handleSuccess, handleError('Error updating address'));
        }

        function CreateOrUpdate(property, user) {
            return $http.post($API.UserModule + '/property/createOrUpdate', {'user': user, 'property': property}).then(handleSuccess, handleError('Error creating/updating property'));
        }

        function Delete(id) {
            return $http.delete('/api/users/' + id).then(handleSuccess, handleError('Error deleting user'));
        }

        function Count(params) {
            return $http.get($API.UserModule + '/user/count' + params + ext).then(handleSuccess, handleError('Error counting all users'));
        }

        function hasRole(user, role) {
            for (var x in user.role) {
                if (user.role[x].authority.toLowerCase() === role.toLowerCase()) {
                    return true;
                }
            }
            return false;
        }

        function findProperty(user, prop) {
            for (var x in user.property) {
                if (user.property[x].label.toLowerCase() === prop.toLowerCase()) {
                    return user.property[x].value;
                }
            }
            return null;
        }

        // private functions

        function handleSuccess(res) {
            if (res.data.id) {
                var user = res.data;
                user.photo = user.photo ? user.photo : defaultAvatar;
                return user;
            }
            return res.data;

        }

        function handleError(res) {
            // return function () {
            return res;
            // };
        }
    }

})();
