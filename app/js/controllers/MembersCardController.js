/**
 * Created by Max on 25/09/16.
 */
(function () {
    'use strict';
    angular
        .module('app')
        .controller('MembersCardController', MembersCardController);

    MembersCardController.$inject = ['ParamService', '$scope', 'WebsocketService', '$rootScope', '$sce'];
    function MembersCardController(ParamService, $scope, WebsocketService, $rootScope, $sce) {
        var vm = this;

        vm.logo = "http://www.aero79.nl/images/logoAero.png";

        vm.cardDesign = "";
        vm.template;
        vm.editorOptions = {
            indentUnit: 4,
            lineWrapping: true,
            lineNumbers: true,
            theme: 'dracula',
            mode: 'htmlmixed'
        };

        vm.save = save;
        vm.print = print;

        vm.getHtml = function (html, returnType) {
            if (!html) {
                return "";
            }
            var matches = html.match(/\{(.[^\s]*)\}/g);
            // \{(\w+[\.*\w+]*)\}
            vm.user = JSON.parse(localStorage.getItem('user'));

            for (var i in matches) {
                var key = matches[i].replace(/\{/g, '').replace(/\}/g, '');
                var value = "";
                value = vm.getValue(key, vm);
                html = html.replace(matches[i], value ? value : '');
            }

            if (returnType && returnType.toLowerCase() === 'html'){
                return html;
            }
            return $sce.trustAsHtml(html);
        };

        vm.getValue = function (path, obj) {
            return path.split('.').reduce(function (prev, curr) {
                return prev ? prev[curr] : undefined
            }, obj)
        };

        initController();

        function initController() {
            loadCurrentTemplate();
        }

        function loadCurrentTemplate() {
            ParamService.GetByLabel("template")
                .then(function (param) {
                    if (param.success === false){
                        ParamService.Create({label: 'template', type: 'text', value: ''})
                            .then(function (result) {
                                vm.template = result;
                                vm.cardDesign = vm.template.value;
                            });
                    } else {
                        vm.template = param;
                        vm.cardDesign = vm.template.value;
                    }
                });
        }

        function save() {
            vm.template.value = vm.cardDesign;
            ParamService.Update(vm.template)
                .then(function (data) {
                    console.log("updated", data);
                });
        }

        function print() {
            var w = window.open();

            $(w.document.head).html('<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">');
            $(w.document.body).html(vm.getHtml(vm.cardDesign, 'html'));
            setTimeout(function () {
                w.print();
                w.close();
            }, 500);
        }

    }

})();
