angular
    .module('app')
    .directive('pageTitle', function () {
        return {
            restrict: 'E',
            transclude: true,
            templateUrl: 'js/directives/title/page-title.html'
        };
    });