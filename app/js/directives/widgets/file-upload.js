angular
    .module('app')
    .directive('uploadChange', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var onChangeFunc = scope.$eval(attrs.uploadChange);
                element.bind('change', onChangeFunc);
            }
        };
    });