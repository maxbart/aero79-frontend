angular
    .module('app')
    .directive('bootstrapDaterangepicker', function ($sce) {
        return {
            restrict: 'C',
            scope: {
                model: '=ngModel'
            },
            link: function (scope, elem, attr) {
                console.log("MODEL", scope.model);
                scope.attrs = attr;
                $(elem).daterangepicker({
                    singleDatePicker: true,
                    locale: {
                        format: 'DD/MM/YYYY'
                    },
                    startDate: scope.model,
                    maxDate: new Date(),
                    showDropdowns: true
                });
                if (!scope.model) {
                    $(elem).val('');
                }
            }
        };
    });