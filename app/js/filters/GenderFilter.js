angular
    .module('app')
    .filter('gender', function () {

        // In the return function, we must pass in a single parameter which will be the data we will work on.
        // We have the ability to support multiple other parameters that can be passed into the filter optionally
        return function (inputEnum, opt1Prefix, opt2LastName) {
            var output = inputEnum;

            if (inputEnum) {
                var gender = inputEnum.toLowerCase();

                output = gender.substr(0, 1).toUpperCase() + gender.substr(1);
            }


            // Do filter work here

            return output;
        }

    });