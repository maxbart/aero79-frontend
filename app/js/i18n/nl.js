var i18n_nl = {
    title: 'Aero \'79',
    greeting: 'Welkom'
};

i18n_nl.nav = {
    title: 'Algemeen',
    dashboard: 'Dashboard',
    members: 'Leden',
    memberCard: 'Ledenpas'
};

i18n_nl.menu = {
    profile: 'Profiel',
    settings: 'Instellingen',
    help: 'Help',
    logOut: 'Uitloggen',
    inbox: {
        seeAll: 'Alle meldingen'
    }
};

i18n_nl.buttons = {
    cancel: 'Annuleer',
    submit: 'Verstuur'
};

i18n_nl.users = {
    index: {
        title: 'Leden',
        panel: {
            list: {
                title: 'Overzicht',
                paragraph: 'Onderstaand vindt u alle leden van modelvliegclub Aero\'79.<br/>Klik op het gewenste lid om meer gegevens in te zien.',
            }
        }
    },
    create: {
        title: 'Nieuw lid',
        panel: {
            subtitle: 'Nieuw',
            wizard: {
                step4: {
                    title: 'Profiel foto'
                },
                step5: {
                    title: 'Overzicht'
                }
            }
        }

    },
    fields: {
        basic: {
            title: 'Persoonsgegevens',
            firstName: 'Voornaam',
            prefixLastName: 'Tussenvoegsels',
            lastName: 'Achternaam',
            fullName: 'Naam',
            dateOfBirth: 'Geboortedatum',
            gender: {
                label: 'Geslacht',
                male: 'Man',
                female: 'Vrouw'
            },
            identification: 'Lidnummer'
        },
        contact: {
            title: 'Contact informatie',
            email: 'Email',
            phone1: 'Tel. 1',
            phone2: 'Tel. 2',
            address: 'Adres',
            street: 'Straat',
            houseNumber: 'Huisnummer',
            houseNumberAddition: 'Huisnummer toevoeging',
            city: 'Stad',
            zipcode: 'Postcode',
            country: 'Land'
        },
        extra: {
            title: 'Extra informatie',
            membershipType: 'Lidmaatschap',
            brevet: {
                label: 'Brevet',
                enginePlane: 'Motorvliegtuig',
                glider: 'Zweefvliegtuig',
                helicopter: 'Helikopter',
                multicopter: 'Multicopter'
            },
        },
        photo: {
            title: 'Profiel Foto'
        }
    },
    profile: {
        title: 'Profiel',
        edit: {
            title: 'Bewerken'
        }
    }
};

i18n_nl.memberCard = {
    title: 'Ledenpas',
    panelEdit: {
        title: 'Bewerk template'
    },
    panelPreview: {
        title: 'Voorbeeld'
    }
};