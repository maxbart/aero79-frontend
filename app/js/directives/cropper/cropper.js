angular
    .module('app')
    .directive('cropper', function ($rootScope) {
        return {
            restrict: 'EC',
            scope: {
                options: "=options"
            },
            transclude: true,
            link: function (scope, elem, attr) {



                // console.log("options", options);

                scope.$watch('options', function (options) {
                    console.log("Watch options", options);

                    scope.attrs = attr;
                    // scope.file = false;
                    var inputElem = $(elem).find("input");
                    var imgElem = $(elem).find("img.source");
                    var previewElem = $(elem).find("img.preview");
                    var buttonElem = $(elem).find(".crop");

                    imgElem.cropper(options);

                    $(elem).find('.zoom').click(function () {
                        imgElem.cropper('zoom', $(this).attr('zoom'));
                    });

                    $rootScope.$on('knob', function (e, args) {
                        console.log("rotating", args);
                        if (args.id == 'cropper-rotation') {
                            imgElem.cropper('rotateTo', args.value);
                        }
                    });

                    if (options.initImg) {
                        imgElem.cropper('replace', options.initImg);
                        previewElem.attr('src', options.initImg);
                        $rootScope.$broadcast('loaded');
                    }

                    buttonElem.click(function () {
                        var data = imgElem.cropper('getCroppedCanvas').toDataURL();
                        previewElem.attr('src', data);
                        $rootScope.$broadcast('crop', {crop: data});
                    });

                    var readUpload = function () {

                        if (this.files && this.files[0]) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                // scope.file = true;
                                imgElem.cropper('replace', e.target.result);
                            };

                            reader.readAsDataURL(this.files[0]);
                            $rootScope.$broadcast('loaded');
                        }
                    };

                    inputElem.bind('change', readUpload);

                });


            },
            templateUrl: 'js/directives/cropper/cropper.html'
        };
    });