/**
 * Created by Max on 25/09/16.
 */

(function () {
    'use strict';

    angular
        .module('app')
        .factory('UserService', UserService);
    UserService.$inject = ['$rootScope', '$http', '$API'];
    function UserService($rootScope, $http, $API) {
        var service = {};
        var ext = ".json";

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.GetByUsername = GetByUsername;
        service.GetByEmail = GetByEmail;
        service.Create = Create;
        service.CreateDetailed = CreateDetailed;
        service.Update = Update;
        service.Delete = Delete;
        service.Count = Count;
        service.hasRole = hasRole;
        service.findProperty = findProperty;

        return service;

        function GetAll() {
            return $http.get($API.UserModule + '/user' + ext).then(handleSuccess, handleError('Error getting all users'));
        }

        function GetById(id) {
            return $http.get($API.UserModule + '/user/' + id + ext).then(handleSuccess, handleError('Error getting user by id'));
        }

        function GetByUsername(username) {
            return $http.get('/api/users/' + username).then(handleSuccess, handleError('Error getting user by username'));
        }

        function GetByEmail(email) {
            return $http.get($API.UserModule + '/user/' + encodeURIComponent(email) + '/byEmail' + ext).then(handleSuccess, handleError('Error getting user by username'));
        }

        function Create(user) {
            return $http.post($API.UserModule + '/user', user).then(handleSuccess, handleError('Error creating user'));
        }

        function CreateDetailed(user, address, extended) {
            return $http.post($API.UserModule + '/user/detailed', {'user': user, 'address': address, 'extended': extended}).then(handleSuccess, handleError);
        }

        function Update(user) {
            return $http.put($API.UserModule + '/user/' + user.id, user).then(handleSuccess, handleError('Error updating user'));
        }

        function Delete(id) {
            return $http.delete($API.UserModule + '/user/' + id).then(handleSuccess, handleError('Error deleting user'));
        }

        function Count(params) {
            return $http.get($API.UserModule + '/user/count' + params + ext).then(handleSuccess, handleError('Error counting all users'));
        }

        function hasRole(user, role) {
            for (var x in user.role) {
                if (user.role[x].authority.toLowerCase() === role.toLowerCase()) {
                    return true;
                }
            }
            return false;
        }

        function findProperty(user, prop) {
            for (var x in user.property) {
                console.log("get " + prop, x, user.property[x]);
                if (user.property[x] && user.property[x].label.toLowerCase() === prop.toLowerCase()) {
                    return user.property[x];
                }
            }
            return null;
        }

        // private functions

        function handleSuccess(res) {
            if (res.data.id) {
                var user = res.data;
                user.photo = user.photo ? user.photo : defaultAvatar;
                return user;
            }
            return res.data;

        }

        function handleError(res) {
            // return function () {
            return res;
            // };
        }
    }

})();
