angular
    .module('app')
    .filter('fullname', function () {

        // In the return function, we must pass in a single parameter which will be the data we will work on.
        // We have the ability to support multiple other parameters that can be passed into the filter optionally
        return function (inputFirstName, opt1Prefix, opt2LastName) {

            // var output = (inputFirstName + " " + opt1Prefix + " " + opt2LastName).replace("  ", " "); //Replace because prefixLastName can be empty causing double whitespace

            var output = inputFirstName;

            if (opt1Prefix) {
                output += " " + opt1Prefix;
            }

            if (opt2LastName) {
                output += " " + opt2LastName;
            }
            // Do filter work here

            return output;
        }

    });