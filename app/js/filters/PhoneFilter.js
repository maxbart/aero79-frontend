angular
    .module('app')
    .filter('phone', function () {
        return function (input, opt1, opt2) {
            var output = "";
            if (!input) {
                return "";
            }
            if (input.startsWith("+")) {
                output = input.replace(/(\+\d{2})(\d{2})(\d{1})(\d{2})(\d{2})(\d*)/g, '($1)$2 $3 $4 $5 $6');
            } else {
                output = input.replace(/(\d{3})(\d{1})(\d{2})(\d{2})(\d*)/g, '$1 $2 $3 $4 $5');
            }
            return output;
        }
    });