/**
 * Created by Max on 25/09/16.
 */

(function () {
    'use strict';

    angular
        .module('app')
        .factory('TemplateService', TemplateService);
    TemplateService.$inject = ['$rootScope', '$http', '$API'];
    function TemplateService($rootScope, $http, $API) {
        var service = {};

        service.parseHTML = parseHTML;

        return service;


        function parseHTML(html, scope) {
            var matches = html.match(/\{(.[^\s]*)\}/g);
            // \{(\w+[\.*\w+]*)\}

            for (var i in matches) {
                var key = matches[i].replace(/\{/g, '').replace(/\}/g, '');
                var value = "";
                value = getValue(key, scope);
                html = html.replace(matches[i], value ? value : '');
            }

            return html;
        }

        function getValue(path, obj) {
            return path.split('.').reduce(function (prev, curr) {
                return prev ? prev[curr] : undefined
            }, obj)
        }

    }

})();
