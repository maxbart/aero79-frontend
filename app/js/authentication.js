/**
 * Created by Max on 25/09/16.
 */

var isLoggedIn = function () {
    var loggedIn = false;
    $.ajax({
        type: "GET",
        async: false,
        headers: {
            "X-Auth-Token": localStorage.getItem('X-Auth-Token')
        },
        url: $API_HOST + $API_USER + "/isAuthenticated"
    })
        .done(function (response) {
            loggedIn = true;
        })
        .fail(function (response) {
            console.error("error", response);
            loggedIn = false;
            window.location.href = "login.html";
        });
    return loggedIn;
};

var authenticate = function (email, password, url) {
    $.ajax({
        type: "POST",
        url: $API_HOST + "/login",
        data: JSON.stringify({"email": email, "password": password}),
        dataType: "json",
        contentType: "application/json"
    })
        .done(function (response) {
            console.log(response);
            localStorage.setItem('X-Auth-Token', response['access_token']);
            localStorage.setItem('user', '{"email":"' + email + '"}');
            window.location.href = url;
        })
        .fail(function (response) {
            console.error("error", response);
        });
};

var logout = function () {
    $.ajax({
        type: "POST",
        url: $API_HOST + "/logout",
        headers: {
            "X-Auth-Token": localStorage.getItem('X-Auth-Token')
        }
    })
        .done(function (response) {
            isLoggedIn();
        })
        .fail(function (response) {
            console.error(response);
        });
};


// $(function () {
//
//     if (window.location.href.indexOf("login") === -1) {
//         isLoggedIn();
//     }
// });