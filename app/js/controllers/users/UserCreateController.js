/**
 * Created by Bart on 25/10/16.
 */
(function () {
    'use strict';

    angular
        .module('app')
        .controller('UserCreateController', UserCreateController);

    UserCreateController.$inject = ['UserService', '$scope', 'WebsocketService', '$rootScope', '$q', '$translate'];
    function UserCreateController(UserService, $scope, WebsocketService, $rootScope, $q, $translate) {
        var vm = this;

        vm.saveUser = saveUser;
        vm.cancel = cancel;
        vm.readUpload = readUpload;
        vm.determineBrevet = determineBrevet;
        vm.user = {};
        vm.address = {};
        vm.extended = {};
        vm.temp = {};

        vm.cropperOptions = {
            aspectRatio: 4 / 4,
            dragMode: 'move'
        };

        $rootScope.$on('crop', function (e, args) {
            vm.user.photo = args.crop;
        });

        vm.wizardOptions = {
            onLeaveStep: leaveAStepCallback,
            includeFinishButton: false,
            reverseButtonsOrder: true,
            hideButtonsOnDisabled: false,
            keyNavigation: false,
            labelNext: '<i class="fa fa-arrow-right"></i>',
            labelPrevious: '<i class="fa fa-arrow-left"></i>'
        };

        initController();

        function initController() {
        }

        function leaveAStepCallback(obj, context) {
            return validateSteps(context.fromStep); // return false to stay on step and true to continue navigation
        }

        function validateSteps(stepNumber) {

            return true;
        }

        function saveUser() {
            vm.user.dateOfBirth = moment(vm.temp.birthday, 'DD/MM/YYYY').toISOString();
            vm.user.password = "test";

            var extended = [];

            for (var key in vm.extended) {
                extended.push({label: key, value: vm.extended[key], type: 'text'});
            }

            var address = Object.keys(vm.address).length > 0 ? vm.address : {};

            UserService.CreateDetailed(vm.user, address, extended)
                .then(function (response) {
                    if (vm.notify) {
                        vm.notify.remove();
                    }
                    console.log("response", response);
                    if (typeof response.status == 'undefined') {
                        window.history.back();
                    } else {
                        console.error("response", response);
                        var text = "Error fields:";

                        text += "<ul>";
                        for (var i in response.data.errors) {
                            text += "<li>" + response.data.errors[i].field + "</li>";
                        }
                        text += "</ul>";
                        vm.notify = new PNotify({
                            title: "Oh Oh! Errors occurred",
                            type: "warning",
                            // text: "Welcome. <br/>Try \nhovering over me. You can click things behind me, because I'm non-blocking.",
                            text: text,
                            // nonblock: {
                            //     nonblock: true
                            // },
                            buttons: {
                                closer: true,
                                sticker: true,
                                show_on_nonblock: true
                            },
                            icon: 'fa fa-exclamation-circle',
                            addclass: 'stack-topleft',
                            styling: 'bootstrap3',
                            hide: false
                        });
                    }
                });
        }

        function determineBrevet() {
            if (vm.temp.brevet) {
                vm.extended.brevet = "";
                vm.extended.brevet = vm.temp.brevet.a ? 'A' : "";
                vm.extended.brevet += vm.temp.brevet.b ? 'B' : "";
                vm.extended.brevet += vm.temp.brevet.c ? 'C' : "";
                vm.extended.brevet += vm.temp.brevet.d ? 'D' : "";
                return vm.extended.brevet;
            }
            return "";
        }

        function cancel() {
            window.history.back();
        }

        function readUpload(input) {
            // var input = this;
            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#cropImage').attr('src', e.target.result);
                };

                reader.readAsDataURL(this.files[0]);
            }
        }
    }

})();