/**
 * Created by Max on 25/09/16.
 */
(function () {
    'use strict';

    angular
        .module('app')
        .controller('DashboardController', DashboardController);

    DashboardController.$inject = ['UserService', '$rootScope', '$scope', 'WebsocketService'];
    function DashboardController(UserService, $rootScope, $scope, WebsocketService) {
        var vm = this;

        // vm.user = null;
        vm.allUsers = [];
        vm.userCount = 0;
        // vm.deleteUser = deleteUser;

        $scope.$on('refreshUsers', function () {
            countUsers();
        });

        initController();

        function initController() {
            // loadAllUsers();
            countUsers();
        }

        function loadCurrentUser() {
            UserService.GetByUsername($rootScope.globals.currentUser.username)
                .then(function (user) {
                    vm.user = user;
                });
        }

        function loadAllUsers() {
            UserService.GetAll()
                .then(function (users) {
                    vm.allUsers = users;
                });
        }

        function countUsers() {
            UserService.Count('/enabled/1')
                .then(function (result) {
                    vm.userCount = result.count;
                });
        }

        function deleteUser(id) {
            UserService.Delete(id)
                .then(function () {
                    loadAllUsers();
                });
        }
    }

})();