var i18n_en = {
    title: 'Aero \'79',
    greeting: 'Hi'
};

i18n_en.nav = {
    title: 'General',
    dashboard: 'Dashboard',
    members: 'Members',
    memberCard: 'Member card'
};

i18n_en.menu = {
    profile: 'Profile',
    settings: 'Settings',
    help: 'Help',
    logOut: 'Sign out',
    inbox: {
        seeAll: 'All alerts'
    }
};

i18n_en.users = {
    index: {
        title: 'Members',
        panel: {
            list: {
                title: 'Overview',
                paragraph: 'Below you find a listing of all members of Aero\'79<br/>Click on a member for more details.',
                table: {
                    id: 'Id',
                    firstName: 'First name',
                    prefixLastName: 'Prefix',
                    lastName: 'Last name'
                }
            }
        }
    }
};